// Copyright 2011 Google Inc. All Rights Reserved.

/**
 * @fileoverview Wrapped methods to send notification to widget from a
 * popup window.
 * @supported Chrome5+, FireFox3.6+, IE8, IE7, and Safari4.0+.
 * @author guibinkong@google.com (Guibin Kong)
 */

// Safety net in case we're used outside google.load()
if (!('google' in window)) {
  window.google = {};
}
if (!('identitytoolkit' in window.google)) {
  window.google.identitytoolkit = {};
}

/**
 * Sends notification to a widget in the parent window. Should be called on the
 * popup window.
 * @param {string} type The type of the notification.
 * @param {Object} opt_params The parameters with the notification.
 * @param {Object=} opt_options Notification options.
 */
window.google.identitytoolkit.notify = function(type, opt_params, opt_options) {
  if (!type) {
    throw 'Illegal parameter: notification type cannot be null.';
  }
  var uuid = opt_options && opt_options.uuid;
  window.opener.google.identitytoolkit.easyrp.util.notifyWidget(type,
      opt_params, uuid);
  var keepPopup = opt_options && opt_options.keepPopup;
  if (!keepPopup) {
    window.close();
  }
};

/**
 * Send 'federatedSuccess' notification to a widget. Should be called on the
 * popup window.
 * @param {Object=} opt_params The parameters with the notification.
 * @param {(number|string|Object)=} opt_options Notification options. If it is a
 *    string or number, it is the uuid for the widget. Otherwise it should be an
 *    object containing uuid, keepPopup and other options.
 */
window.google.identitytoolkit.notifyFederatedSuccess =
    function(opt_params, opt_options) {
  if (typeof opt_options === 'string' || typeof opt_options === 'number') {
    opt_options = {uuid: opt_options};
  }
  window.google.identitytoolkit.notify('federatedSuccess', opt_params,
      opt_options);
};

/**
 * Send 'federatedError' notification to a widget. Should be called on the
 * popup window.
 * @param {string=} opt_errorType The error type.
 * @param {Object=} opt_params The parameters with the notification.
 * @param {(number|string|Object)=} opt_options Notification options. If it is a
 *    string or number, it is the uuid for the widget. Otherwise it should be an
 *    object containing uuid, keepPopup and other options.
 */
window.google.identitytoolkit.notifyFederatedError = function(opt_errorType,
    opt_params, opt_options) {
  var params = opt_params ? opt_params : {};
  params['errorType'] = opt_errorType;
  if (typeof opt_options === 'string' || typeof opt_options === 'number') {
    opt_options = {uuid: opt_options};
  }
  window.google.identitytoolkit.notify('federatedError', params, opt_options);
};
